package com.neu.test.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.neu.test.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class StatesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(States.class);
        States states1 = new States();
        states1.setId(1L);
        States states2 = new States();
        states2.setId(states1.getId());
        assertThat(states1).isEqualTo(states2);
        states2.setId(2L);
        assertThat(states1).isNotEqualTo(states2);
        states1.setId(null);
        assertThat(states1).isNotEqualTo(states2);
    }
}
