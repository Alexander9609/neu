package com.neu.test.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.neu.test.IntegrationTest;
import com.neu.test.domain.Cities;
import com.neu.test.domain.States;
import com.neu.test.repository.CitiesRepository;
import com.neu.test.service.criteria.CitiesCriteria;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CitiesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CitiesResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_POPULATION = "AAAAAAAAAA";
    private static final String UPDATED_POPULATION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/cities";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CitiesRepository citiesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCitiesMockMvc;

    private Cities cities;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cities createEntity(EntityManager em) {
        Cities cities = new Cities().name(DEFAULT_NAME).population(DEFAULT_POPULATION);
        return cities;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cities createUpdatedEntity(EntityManager em) {
        Cities cities = new Cities().name(UPDATED_NAME).population(UPDATED_POPULATION);
        return cities;
    }

    @BeforeEach
    public void initTest() {
        cities = createEntity(em);
    }

    @Test
    @Transactional
    void createCities() throws Exception {
        int databaseSizeBeforeCreate = citiesRepository.findAll().size();
        // Create the Cities
        restCitiesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cities)))
            .andExpect(status().isCreated());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeCreate + 1);
        Cities testCities = citiesList.get(citiesList.size() - 1);
        assertThat(testCities.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCities.getPopulation()).isEqualTo(DEFAULT_POPULATION);
    }

    @Test
    @Transactional
    void createCitiesWithExistingId() throws Exception {
        // Create the Cities with an existing ID
        cities.setId(1L);

        int databaseSizeBeforeCreate = citiesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCitiesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cities)))
            .andExpect(status().isBadRequest());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCities() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList
        restCitiesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cities.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].population").value(hasItem(DEFAULT_POPULATION)));
    }

    @Test
    @Transactional
    void getCities() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get the cities
        restCitiesMockMvc
            .perform(get(ENTITY_API_URL_ID, cities.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cities.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.population").value(DEFAULT_POPULATION));
    }

    @Test
    @Transactional
    void getCitiesByIdFiltering() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        Long id = cities.getId();

        defaultCitiesShouldBeFound("id.equals=" + id);
        defaultCitiesShouldNotBeFound("id.notEquals=" + id);

        defaultCitiesShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCitiesShouldNotBeFound("id.greaterThan=" + id);

        defaultCitiesShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCitiesShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCitiesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where name equals to DEFAULT_NAME
        defaultCitiesShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the citiesList where name equals to UPDATED_NAME
        defaultCitiesShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCitiesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where name not equals to DEFAULT_NAME
        defaultCitiesShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the citiesList where name not equals to UPDATED_NAME
        defaultCitiesShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCitiesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCitiesShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the citiesList where name equals to UPDATED_NAME
        defaultCitiesShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCitiesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where name is not null
        defaultCitiesShouldBeFound("name.specified=true");

        // Get all the citiesList where name is null
        defaultCitiesShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllCitiesByNameContainsSomething() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where name contains DEFAULT_NAME
        defaultCitiesShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the citiesList where name contains UPDATED_NAME
        defaultCitiesShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCitiesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where name does not contain DEFAULT_NAME
        defaultCitiesShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the citiesList where name does not contain UPDATED_NAME
        defaultCitiesShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCitiesByPopulationIsEqualToSomething() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where population equals to DEFAULT_POPULATION
        defaultCitiesShouldBeFound("population.equals=" + DEFAULT_POPULATION);

        // Get all the citiesList where population equals to UPDATED_POPULATION
        defaultCitiesShouldNotBeFound("population.equals=" + UPDATED_POPULATION);
    }

    @Test
    @Transactional
    void getAllCitiesByPopulationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where population not equals to DEFAULT_POPULATION
        defaultCitiesShouldNotBeFound("population.notEquals=" + DEFAULT_POPULATION);

        // Get all the citiesList where population not equals to UPDATED_POPULATION
        defaultCitiesShouldBeFound("population.notEquals=" + UPDATED_POPULATION);
    }

    @Test
    @Transactional
    void getAllCitiesByPopulationIsInShouldWork() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where population in DEFAULT_POPULATION or UPDATED_POPULATION
        defaultCitiesShouldBeFound("population.in=" + DEFAULT_POPULATION + "," + UPDATED_POPULATION);

        // Get all the citiesList where population equals to UPDATED_POPULATION
        defaultCitiesShouldNotBeFound("population.in=" + UPDATED_POPULATION);
    }

    @Test
    @Transactional
    void getAllCitiesByPopulationIsNullOrNotNull() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where population is not null
        defaultCitiesShouldBeFound("population.specified=true");

        // Get all the citiesList where population is null
        defaultCitiesShouldNotBeFound("population.specified=false");
    }

    @Test
    @Transactional
    void getAllCitiesByPopulationContainsSomething() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where population contains DEFAULT_POPULATION
        defaultCitiesShouldBeFound("population.contains=" + DEFAULT_POPULATION);

        // Get all the citiesList where population contains UPDATED_POPULATION
        defaultCitiesShouldNotBeFound("population.contains=" + UPDATED_POPULATION);
    }

    @Test
    @Transactional
    void getAllCitiesByPopulationNotContainsSomething() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        // Get all the citiesList where population does not contain DEFAULT_POPULATION
        defaultCitiesShouldNotBeFound("population.doesNotContain=" + DEFAULT_POPULATION);

        // Get all the citiesList where population does not contain UPDATED_POPULATION
        defaultCitiesShouldBeFound("population.doesNotContain=" + UPDATED_POPULATION);
    }

    @Test
    @Transactional
    void getAllCitiesByIdStateIsEqualToSomething() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);
        States idState = StatesResourceIT.createEntity(em);
        em.persist(idState);
        em.flush();
        cities.setIdState(idState);
        citiesRepository.saveAndFlush(cities);
        Long idStateId = idState.getId();

        // Get all the citiesList where idState equals to idStateId
        defaultCitiesShouldBeFound("idStateId.equals=" + idStateId);

        // Get all the citiesList where idState equals to (idStateId + 1)
        defaultCitiesShouldNotBeFound("idStateId.equals=" + (idStateId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCitiesShouldBeFound(String filter) throws Exception {
        restCitiesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cities.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].population").value(hasItem(DEFAULT_POPULATION)));

        // Check, that the count call also returns 1
        restCitiesMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCitiesShouldNotBeFound(String filter) throws Exception {
        restCitiesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCitiesMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCities() throws Exception {
        // Get the cities
        restCitiesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCities() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        int databaseSizeBeforeUpdate = citiesRepository.findAll().size();

        // Update the cities
        Cities updatedCities = citiesRepository.findById(cities.getId()).get();
        // Disconnect from session so that the updates on updatedCities are not directly saved in db
        em.detach(updatedCities);
        updatedCities.name(UPDATED_NAME).population(UPDATED_POPULATION);

        restCitiesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCities.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCities))
            )
            .andExpect(status().isOk());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeUpdate);
        Cities testCities = citiesList.get(citiesList.size() - 1);
        assertThat(testCities.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCities.getPopulation()).isEqualTo(UPDATED_POPULATION);
    }

    @Test
    @Transactional
    void putNonExistingCities() throws Exception {
        int databaseSizeBeforeUpdate = citiesRepository.findAll().size();
        cities.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCitiesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, cities.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cities))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCities() throws Exception {
        int databaseSizeBeforeUpdate = citiesRepository.findAll().size();
        cities.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCitiesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cities))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCities() throws Exception {
        int databaseSizeBeforeUpdate = citiesRepository.findAll().size();
        cities.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCitiesMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cities)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCitiesWithPatch() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        int databaseSizeBeforeUpdate = citiesRepository.findAll().size();

        // Update the cities using partial update
        Cities partialUpdatedCities = new Cities();
        partialUpdatedCities.setId(cities.getId());

        restCitiesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCities.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCities))
            )
            .andExpect(status().isOk());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeUpdate);
        Cities testCities = citiesList.get(citiesList.size() - 1);
        assertThat(testCities.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCities.getPopulation()).isEqualTo(DEFAULT_POPULATION);
    }

    @Test
    @Transactional
    void fullUpdateCitiesWithPatch() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        int databaseSizeBeforeUpdate = citiesRepository.findAll().size();

        // Update the cities using partial update
        Cities partialUpdatedCities = new Cities();
        partialUpdatedCities.setId(cities.getId());

        partialUpdatedCities.name(UPDATED_NAME).population(UPDATED_POPULATION);

        restCitiesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCities.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCities))
            )
            .andExpect(status().isOk());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeUpdate);
        Cities testCities = citiesList.get(citiesList.size() - 1);
        assertThat(testCities.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCities.getPopulation()).isEqualTo(UPDATED_POPULATION);
    }

    @Test
    @Transactional
    void patchNonExistingCities() throws Exception {
        int databaseSizeBeforeUpdate = citiesRepository.findAll().size();
        cities.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCitiesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, cities.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(cities))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCities() throws Exception {
        int databaseSizeBeforeUpdate = citiesRepository.findAll().size();
        cities.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCitiesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(cities))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCities() throws Exception {
        int databaseSizeBeforeUpdate = citiesRepository.findAll().size();
        cities.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCitiesMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(cities)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Cities in the database
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCities() throws Exception {
        // Initialize the database
        citiesRepository.saveAndFlush(cities);

        int databaseSizeBeforeDelete = citiesRepository.findAll().size();

        // Delete the cities
        restCitiesMockMvc
            .perform(delete(ENTITY_API_URL_ID, cities.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cities> citiesList = citiesRepository.findAll();
        assertThat(citiesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
