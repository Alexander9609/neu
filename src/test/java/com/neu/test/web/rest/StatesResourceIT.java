package com.neu.test.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.neu.test.IntegrationTest;
import com.neu.test.domain.Country;
import com.neu.test.domain.States;
import com.neu.test.repository.StatesRepository;
import com.neu.test.service.criteria.StatesCriteria;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link StatesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class StatesResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/states";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private StatesRepository statesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStatesMockMvc;

    private States states;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static States createEntity(EntityManager em) {
        States states = new States().name(DEFAULT_NAME);
        return states;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static States createUpdatedEntity(EntityManager em) {
        States states = new States().name(UPDATED_NAME);
        return states;
    }

    @BeforeEach
    public void initTest() {
        states = createEntity(em);
    }

    @Test
    @Transactional
    void createStates() throws Exception {
        int databaseSizeBeforeCreate = statesRepository.findAll().size();
        // Create the States
        restStatesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(states)))
            .andExpect(status().isCreated());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeCreate + 1);
        States testStates = statesList.get(statesList.size() - 1);
        assertThat(testStates.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createStatesWithExistingId() throws Exception {
        // Create the States with an existing ID
        states.setId(1L);

        int databaseSizeBeforeCreate = statesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restStatesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(states)))
            .andExpect(status().isBadRequest());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllStates() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get all the statesList
        restStatesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(states.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getStates() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get the states
        restStatesMockMvc
            .perform(get(ENTITY_API_URL_ID, states.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(states.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getStatesByIdFiltering() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        Long id = states.getId();

        defaultStatesShouldBeFound("id.equals=" + id);
        defaultStatesShouldNotBeFound("id.notEquals=" + id);

        defaultStatesShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultStatesShouldNotBeFound("id.greaterThan=" + id);

        defaultStatesShouldBeFound("id.lessThanOrEqual=" + id);
        defaultStatesShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllStatesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get all the statesList where name equals to DEFAULT_NAME
        defaultStatesShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the statesList where name equals to UPDATED_NAME
        defaultStatesShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllStatesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get all the statesList where name not equals to DEFAULT_NAME
        defaultStatesShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the statesList where name not equals to UPDATED_NAME
        defaultStatesShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllStatesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get all the statesList where name in DEFAULT_NAME or UPDATED_NAME
        defaultStatesShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the statesList where name equals to UPDATED_NAME
        defaultStatesShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllStatesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get all the statesList where name is not null
        defaultStatesShouldBeFound("name.specified=true");

        // Get all the statesList where name is null
        defaultStatesShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllStatesByNameContainsSomething() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get all the statesList where name contains DEFAULT_NAME
        defaultStatesShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the statesList where name contains UPDATED_NAME
        defaultStatesShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllStatesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get all the statesList where name does not contain DEFAULT_NAME
        defaultStatesShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the statesList where name does not contain UPDATED_NAME
        defaultStatesShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllStatesByIdCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);
        Country idCountry = CountryResourceIT.createEntity(em);
        em.persist(idCountry);
        em.flush();
        states.setIdCountry(idCountry);
        statesRepository.saveAndFlush(states);
        Long idCountryId = idCountry.getId();

        // Get all the statesList where idCountry equals to idCountryId
        defaultStatesShouldBeFound("idCountryId.equals=" + idCountryId);

        // Get all the statesList where idCountry equals to (idCountryId + 1)
        defaultStatesShouldNotBeFound("idCountryId.equals=" + (idCountryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultStatesShouldBeFound(String filter) throws Exception {
        restStatesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(states.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restStatesMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultStatesShouldNotBeFound(String filter) throws Exception {
        restStatesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restStatesMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingStates() throws Exception {
        // Get the states
        restStatesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewStates() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        int databaseSizeBeforeUpdate = statesRepository.findAll().size();

        // Update the states
        States updatedStates = statesRepository.findById(states.getId()).get();
        // Disconnect from session so that the updates on updatedStates are not directly saved in db
        em.detach(updatedStates);
        updatedStates.name(UPDATED_NAME);

        restStatesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedStates.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedStates))
            )
            .andExpect(status().isOk());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
        States testStates = statesList.get(statesList.size() - 1);
        assertThat(testStates.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingStates() throws Exception {
        int databaseSizeBeforeUpdate = statesRepository.findAll().size();
        states.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStatesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, states.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(states))
            )
            .andExpect(status().isBadRequest());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchStates() throws Exception {
        int databaseSizeBeforeUpdate = statesRepository.findAll().size();
        states.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(states))
            )
            .andExpect(status().isBadRequest());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamStates() throws Exception {
        int databaseSizeBeforeUpdate = statesRepository.findAll().size();
        states.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatesMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(states)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateStatesWithPatch() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        int databaseSizeBeforeUpdate = statesRepository.findAll().size();

        // Update the states using partial update
        States partialUpdatedStates = new States();
        partialUpdatedStates.setId(states.getId());

        restStatesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedStates.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedStates))
            )
            .andExpect(status().isOk());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
        States testStates = statesList.get(statesList.size() - 1);
        assertThat(testStates.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void fullUpdateStatesWithPatch() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        int databaseSizeBeforeUpdate = statesRepository.findAll().size();

        // Update the states using partial update
        States partialUpdatedStates = new States();
        partialUpdatedStates.setId(states.getId());

        partialUpdatedStates.name(UPDATED_NAME);

        restStatesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedStates.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedStates))
            )
            .andExpect(status().isOk());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
        States testStates = statesList.get(statesList.size() - 1);
        assertThat(testStates.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingStates() throws Exception {
        int databaseSizeBeforeUpdate = statesRepository.findAll().size();
        states.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStatesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, states.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(states))
            )
            .andExpect(status().isBadRequest());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchStates() throws Exception {
        int databaseSizeBeforeUpdate = statesRepository.findAll().size();
        states.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(states))
            )
            .andExpect(status().isBadRequest());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamStates() throws Exception {
        int databaseSizeBeforeUpdate = statesRepository.findAll().size();
        states.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatesMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(states)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteStates() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        int databaseSizeBeforeDelete = statesRepository.findAll().size();

        // Delete the states
        restStatesMockMvc
            .perform(delete(ENTITY_API_URL_ID, states.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
