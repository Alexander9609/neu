import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { StatesComponent } from './list/states.component';
import { StatesDetailComponent } from './detail/states-detail.component';
import { StatesUpdateComponent } from './update/states-update.component';
import { StatesDeleteDialogComponent } from './delete/states-delete-dialog.component';
import { StatesRoutingModule } from './route/states-routing.module';

@NgModule({
  imports: [SharedModule, StatesRoutingModule],
  declarations: [StatesComponent, StatesDetailComponent, StatesUpdateComponent, StatesDeleteDialogComponent],
  entryComponents: [StatesDeleteDialogComponent],
})
export class StatesModule {}
