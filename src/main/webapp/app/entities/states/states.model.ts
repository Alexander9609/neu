import { ICountry } from 'app/entities/country/country.model';

export interface IStates {
  id?: number;
  name?: string | null;
  idCountry?: ICountry | null;
}

export class States implements IStates {
  constructor(public id?: number, public name?: string | null, public idCountry?: ICountry | null) {}
}

export function getStatesIdentifier(states: IStates): number | undefined {
  return states.id;
}
