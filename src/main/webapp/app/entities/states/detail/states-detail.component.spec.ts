import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { StatesDetailComponent } from './states-detail.component';

describe('Component Tests', () => {
  describe('States Management Detail Component', () => {
    let comp: StatesDetailComponent;
    let fixture: ComponentFixture<StatesDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [StatesDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ states: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(StatesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(StatesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load states on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.states).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
