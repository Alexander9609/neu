import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IStates, States } from '../states.model';

import { StatesService } from './states.service';

describe('Service Tests', () => {
  describe('States Service', () => {
    let service: StatesService;
    let httpMock: HttpTestingController;
    let elemDefault: IStates;
    let expectedResult: IStates | IStates[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(StatesService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        name: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a States', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new States()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a States', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a States', () => {
        const patchObject = Object.assign({}, new States());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of States', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a States', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addStatesToCollectionIfMissing', () => {
        it('should add a States to an empty array', () => {
          const states: IStates = { id: 123 };
          expectedResult = service.addStatesToCollectionIfMissing([], states);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(states);
        });

        it('should not add a States to an array that contains it', () => {
          const states: IStates = { id: 123 };
          const statesCollection: IStates[] = [
            {
              ...states,
            },
            { id: 456 },
          ];
          expectedResult = service.addStatesToCollectionIfMissing(statesCollection, states);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a States to an array that doesn't contain it", () => {
          const states: IStates = { id: 123 };
          const statesCollection: IStates[] = [{ id: 456 }];
          expectedResult = service.addStatesToCollectionIfMissing(statesCollection, states);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(states);
        });

        it('should add only unique States to an array', () => {
          const statesArray: IStates[] = [{ id: 123 }, { id: 456 }, { id: 56992 }];
          const statesCollection: IStates[] = [{ id: 123 }];
          expectedResult = service.addStatesToCollectionIfMissing(statesCollection, ...statesArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const states: IStates = { id: 123 };
          const states2: IStates = { id: 456 };
          expectedResult = service.addStatesToCollectionIfMissing([], states, states2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(states);
          expect(expectedResult).toContain(states2);
        });

        it('should accept null and undefined values', () => {
          const states: IStates = { id: 123 };
          expectedResult = service.addStatesToCollectionIfMissing([], null, states, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(states);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
