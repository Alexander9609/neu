import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IStates, getStatesIdentifier } from '../states.model';

export type EntityResponseType = HttpResponse<IStates>;
export type EntityArrayResponseType = HttpResponse<IStates[]>;

@Injectable({ providedIn: 'root' })
export class StatesService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/states');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(states: IStates): Observable<EntityResponseType> {
    return this.http.post<IStates>(this.resourceUrl, states, { observe: 'response' });
  }

  update(states: IStates): Observable<EntityResponseType> {
    return this.http.put<IStates>(`${this.resourceUrl}/${getStatesIdentifier(states) as number}`, states, { observe: 'response' });
  }

  partialUpdate(states: IStates): Observable<EntityResponseType> {
    return this.http.patch<IStates>(`${this.resourceUrl}/${getStatesIdentifier(states) as number}`, states, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IStates>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IStates[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addStatesToCollectionIfMissing(statesCollection: IStates[], ...statesToCheck: (IStates | null | undefined)[]): IStates[] {
    const states: IStates[] = statesToCheck.filter(isPresent);
    if (states.length > 0) {
      const statesCollectionIdentifiers = statesCollection.map(statesItem => getStatesIdentifier(statesItem)!);
      const statesToAdd = states.filter(statesItem => {
        const statesIdentifier = getStatesIdentifier(statesItem);
        if (statesIdentifier == null || statesCollectionIdentifiers.includes(statesIdentifier)) {
          return false;
        }
        statesCollectionIdentifiers.push(statesIdentifier);
        return true;
      });
      return [...statesToAdd, ...statesCollection];
    }
    return statesCollection;
  }
}
