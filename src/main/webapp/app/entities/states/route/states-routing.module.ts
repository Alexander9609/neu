import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { StatesComponent } from '../list/states.component';
import { StatesDetailComponent } from '../detail/states-detail.component';
import { StatesUpdateComponent } from '../update/states-update.component';
import { StatesRoutingResolveService } from './states-routing-resolve.service';

const statesRoute: Routes = [
  {
    path: '',
    component: StatesComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: StatesDetailComponent,
    resolve: {
      states: StatesRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: StatesUpdateComponent,
    resolve: {
      states: StatesRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: StatesUpdateComponent,
    resolve: {
      states: StatesRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(statesRoute)],
  exports: [RouterModule],
})
export class StatesRoutingModule {}
