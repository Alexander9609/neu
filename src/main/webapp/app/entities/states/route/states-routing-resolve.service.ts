import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IStates, States } from '../states.model';
import { StatesService } from '../service/states.service';

@Injectable({ providedIn: 'root' })
export class StatesRoutingResolveService implements Resolve<IStates> {
  constructor(protected service: StatesService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStates> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((states: HttpResponse<States>) => {
          if (states.body) {
            return of(states.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new States());
  }
}
