import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IStates } from '../states.model';
import { StatesService } from '../service/states.service';

@Component({
  templateUrl: './states-delete-dialog.component.html',
})
export class StatesDeleteDialogComponent {
  states?: IStates;

  constructor(protected statesService: StatesService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.statesService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
