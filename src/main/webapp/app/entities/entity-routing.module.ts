import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'country',
        data: { pageTitle: 'neuApp.country.home.title' },
        loadChildren: () => import('./country/country.module').then(m => m.CountryModule),
      },
      {
        path: 'states',
        data: { pageTitle: 'neuApp.states.home.title' },
        loadChildren: () => import('./states/states.module').then(m => m.StatesModule),
      },
      {
        path: 'cities',
        data: { pageTitle: 'neuApp.cities.home.title' },
        loadChildren: () => import('./cities/cities.module').then(m => m.CitiesModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
