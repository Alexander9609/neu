import { IStates } from 'app/entities/states/states.model';

export interface ICities {
  id?: number;
  name?: string | null;
  population?: string | null;
  idState?: IStates | null;
}

export class Cities implements ICities {
  constructor(public id?: number, public name?: string | null, public population?: string | null, public idState?: IStates | null) {}
}

export function getCitiesIdentifier(cities: ICities): number | undefined {
  return cities.id;
}
