import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICities, Cities } from '../cities.model';
import { CitiesService } from '../service/cities.service';
import { IStates } from 'app/entities/states/states.model';
import { StatesService } from 'app/entities/states/service/states.service';

@Component({
  selector: 'jhi-cities-update',
  templateUrl: './cities-update.component.html',
})
export class CitiesUpdateComponent implements OnInit {
  isSaving = false;

  statesSharedCollection: IStates[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    population: [],
    idState: [],
  });

  constructor(
    protected citiesService: CitiesService,
    protected statesService: StatesService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cities }) => {
      this.updateForm(cities);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cities = this.createFromForm();
    if (cities.id !== undefined) {
      this.subscribeToSaveResponse(this.citiesService.update(cities));
    } else {
      this.subscribeToSaveResponse(this.citiesService.create(cities));
    }
  }

  trackStatesById(index: number, item: IStates): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICities>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(cities: ICities): void {
    this.editForm.patchValue({
      id: cities.id,
      name: cities.name,
      population: cities.population,
      idState: cities.idState,
    });

    this.statesSharedCollection = this.statesService.addStatesToCollectionIfMissing(this.statesSharedCollection, cities.idState);
  }

  protected loadRelationshipsOptions(): void {
    this.statesService
      .query()
      .pipe(map((res: HttpResponse<IStates[]>) => res.body ?? []))
      .pipe(map((states: IStates[]) => this.statesService.addStatesToCollectionIfMissing(states, this.editForm.get('idState')!.value)))
      .subscribe((states: IStates[]) => (this.statesSharedCollection = states));
  }

  protected createFromForm(): ICities {
    return {
      ...new Cities(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      population: this.editForm.get(['population'])!.value,
      idState: this.editForm.get(['idState'])!.value,
    };
  }
}
