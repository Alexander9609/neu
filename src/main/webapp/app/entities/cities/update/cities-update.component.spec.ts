jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CitiesService } from '../service/cities.service';
import { ICities, Cities } from '../cities.model';
import { IStates } from 'app/entities/states/states.model';
import { StatesService } from 'app/entities/states/service/states.service';

import { CitiesUpdateComponent } from './cities-update.component';

describe('Component Tests', () => {
  describe('Cities Management Update Component', () => {
    let comp: CitiesUpdateComponent;
    let fixture: ComponentFixture<CitiesUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let citiesService: CitiesService;
    let statesService: StatesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CitiesUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(CitiesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CitiesUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      citiesService = TestBed.inject(CitiesService);
      statesService = TestBed.inject(StatesService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call States query and add missing value', () => {
        const cities: ICities = { id: 456 };
        const idState: IStates = { id: 36253 };
        cities.idState = idState;

        const statesCollection: IStates[] = [{ id: 42061 }];
        spyOn(statesService, 'query').and.returnValue(of(new HttpResponse({ body: statesCollection })));
        const additionalStates = [idState];
        const expectedCollection: IStates[] = [...additionalStates, ...statesCollection];
        spyOn(statesService, 'addStatesToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ cities });
        comp.ngOnInit();

        expect(statesService.query).toHaveBeenCalled();
        expect(statesService.addStatesToCollectionIfMissing).toHaveBeenCalledWith(statesCollection, ...additionalStates);
        expect(comp.statesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const cities: ICities = { id: 456 };
        const idState: IStates = { id: 3686 };
        cities.idState = idState;

        activatedRoute.data = of({ cities });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(cities));
        expect(comp.statesSharedCollection).toContain(idState);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const cities = { id: 123 };
        spyOn(citiesService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ cities });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: cities }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(citiesService.update).toHaveBeenCalledWith(cities);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const cities = new Cities();
        spyOn(citiesService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ cities });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: cities }));
        saveSubject.complete();

        // THEN
        expect(citiesService.create).toHaveBeenCalledWith(cities);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const cities = { id: 123 };
        spyOn(citiesService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ cities });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(citiesService.update).toHaveBeenCalledWith(cities);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackStatesById', () => {
        it('Should return tracked States primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackStatesById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
