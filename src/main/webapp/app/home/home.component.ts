import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { StatesService } from 'app/entities/states/service/states.service';
import { CitiesService } from 'app/entities/cities/service/cities.service';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  searchValues: any;

  countries: any;
  constructor(
    private accountService: AccountService,
    private router: Router,
    private serviceCountry: CountryService,
    private serviceStates: StatesService,
    private serviceCities: CitiesService
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
  }

  getCountries(): void {
    // eslint-disable-next-line no-console
    console.log(this.searchValues);

    this.serviceStates.query().subscribe(succes => {
      this.countries = succes.body;
      // eslint-disable-next-line no-console
      console.log(this.countries);
    });
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
}
