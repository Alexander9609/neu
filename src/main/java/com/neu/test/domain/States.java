package com.neu.test.domain;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A States.
 */
@Entity
@Table(name = "states")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class States implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    private Country idCountry;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public States id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public States name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getIdCountry() {
        return this.idCountry;
    }

    public States idCountry(Country country) {
        this.setIdCountry(country);
        return this;
    }

    public void setIdCountry(Country country) {
        this.idCountry = country;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof States)) {
            return false;
        }
        return id != null && id.equals(((States) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "States{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
