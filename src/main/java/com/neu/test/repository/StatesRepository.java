package com.neu.test.repository;

import com.neu.test.domain.States;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the States entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StatesRepository extends JpaRepository<States, Long>, JpaSpecificationExecutor<States> {}
