package com.neu.test.service;

import com.neu.test.domain.States;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link States}.
 */
public interface StatesService {
    /**
     * Save a states.
     *
     * @param states the entity to save.
     * @return the persisted entity.
     */
    States save(States states);

    /**
     * Partially updates a states.
     *
     * @param states the entity to update partially.
     * @return the persisted entity.
     */
    Optional<States> partialUpdate(States states);

    /**
     * Get all the states.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<States> findAll(Pageable pageable);

    /**
     * Get the "id" states.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<States> findOne(Long id);

    /**
     * Delete the "id" states.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
