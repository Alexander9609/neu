package com.neu.test.service;

import com.neu.test.domain.*; // for static metamodels
import com.neu.test.domain.Cities;
import com.neu.test.repository.CitiesRepository;
import com.neu.test.service.criteria.CitiesCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Cities} entities in the database.
 * The main input is a {@link CitiesCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Cities} or a {@link Page} of {@link Cities} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CitiesQueryService extends QueryService<Cities> {

    private final Logger log = LoggerFactory.getLogger(CitiesQueryService.class);

    private final CitiesRepository citiesRepository;

    public CitiesQueryService(CitiesRepository citiesRepository) {
        this.citiesRepository = citiesRepository;
    }

    /**
     * Return a {@link List} of {@link Cities} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Cities> findByCriteria(CitiesCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Cities> specification = createSpecification(criteria);
        return citiesRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Cities} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Cities> findByCriteria(CitiesCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Cities> specification = createSpecification(criteria);
        return citiesRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CitiesCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Cities> specification = createSpecification(criteria);
        return citiesRepository.count(specification);
    }

    /**
     * Function to convert {@link CitiesCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Cities> createSpecification(CitiesCriteria criteria) {
        Specification<Cities> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Cities_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Cities_.name));
            }
            if (criteria.getPopulation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPopulation(), Cities_.population));
            }
            if (criteria.getIdStateId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getIdStateId(), root -> root.join(Cities_.idState, JoinType.LEFT).get(States_.id))
                    );
            }
        }
        return specification;
    }
}
