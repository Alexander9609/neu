package com.neu.test.service;

import com.neu.test.domain.*; // for static metamodels
import com.neu.test.domain.States;
import com.neu.test.repository.StatesRepository;
import com.neu.test.service.criteria.StatesCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link States} entities in the database.
 * The main input is a {@link StatesCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link States} or a {@link Page} of {@link States} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StatesQueryService extends QueryService<States> {

    private final Logger log = LoggerFactory.getLogger(StatesQueryService.class);

    private final StatesRepository statesRepository;

    public StatesQueryService(StatesRepository statesRepository) {
        this.statesRepository = statesRepository;
    }

    /**
     * Return a {@link List} of {@link States} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<States> findByCriteria(StatesCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<States> specification = createSpecification(criteria);
        return statesRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link States} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<States> findByCriteria(StatesCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<States> specification = createSpecification(criteria);
        return statesRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(StatesCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<States> specification = createSpecification(criteria);
        return statesRepository.count(specification);
    }

    /**
     * Function to convert {@link StatesCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<States> createSpecification(StatesCriteria criteria) {
        Specification<States> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), States_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), States_.name));
            }
            if (criteria.getIdCountryId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getIdCountryId(), root -> root.join(States_.idCountry, JoinType.LEFT).get(Country_.id))
                    );
            }
        }
        return specification;
    }
}
