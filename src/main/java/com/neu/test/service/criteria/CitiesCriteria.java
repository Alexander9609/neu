package com.neu.test.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.neu.test.domain.Cities} entity. This class is used
 * in {@link com.neu.test.web.rest.CitiesResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /cities?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CitiesCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter population;

    private LongFilter idStateId;

    public CitiesCriteria() {}

    public CitiesCriteria(CitiesCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.population = other.population == null ? null : other.population.copy();
        this.idStateId = other.idStateId == null ? null : other.idStateId.copy();
    }

    @Override
    public CitiesCriteria copy() {
        return new CitiesCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getPopulation() {
        return population;
    }

    public StringFilter population() {
        if (population == null) {
            population = new StringFilter();
        }
        return population;
    }

    public void setPopulation(StringFilter population) {
        this.population = population;
    }

    public LongFilter getIdStateId() {
        return idStateId;
    }

    public LongFilter idStateId() {
        if (idStateId == null) {
            idStateId = new LongFilter();
        }
        return idStateId;
    }

    public void setIdStateId(LongFilter idStateId) {
        this.idStateId = idStateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CitiesCriteria that = (CitiesCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(population, that.population) &&
            Objects.equals(idStateId, that.idStateId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, population, idStateId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CitiesCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (population != null ? "population=" + population + ", " : "") +
            (idStateId != null ? "idStateId=" + idStateId + ", " : "") +
            "}";
    }
}
