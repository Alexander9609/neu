package com.neu.test.service.impl;

import com.neu.test.domain.States;
import com.neu.test.repository.StatesRepository;
import com.neu.test.service.StatesService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link States}.
 */
@Service
@Transactional
public class StatesServiceImpl implements StatesService {

    private final Logger log = LoggerFactory.getLogger(StatesServiceImpl.class);

    private final StatesRepository statesRepository;

    public StatesServiceImpl(StatesRepository statesRepository) {
        this.statesRepository = statesRepository;
    }

    @Override
    public States save(States states) {
        log.debug("Request to save States : {}", states);
        return statesRepository.save(states);
    }

    @Override
    public Optional<States> partialUpdate(States states) {
        log.debug("Request to partially update States : {}", states);

        return statesRepository
            .findById(states.getId())
            .map(
                existingStates -> {
                    if (states.getName() != null) {
                        existingStates.setName(states.getName());
                    }

                    return existingStates;
                }
            )
            .map(statesRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<States> findAll(Pageable pageable) {
        log.debug("Request to get all States");
        return statesRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<States> findOne(Long id) {
        log.debug("Request to get States : {}", id);
        return statesRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete States : {}", id);
        statesRepository.deleteById(id);
    }
}
