package com.neu.test.web.rest;

import com.neu.test.domain.States;
import com.neu.test.repository.StatesRepository;
import com.neu.test.service.StatesQueryService;
import com.neu.test.service.StatesService;
import com.neu.test.service.criteria.StatesCriteria;
import com.neu.test.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.neu.test.domain.States}.
 */
@RestController
@RequestMapping("/api")
public class StatesResource {

    private final Logger log = LoggerFactory.getLogger(StatesResource.class);

    private static final String ENTITY_NAME = "states";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StatesService statesService;

    private final StatesRepository statesRepository;

    private final StatesQueryService statesQueryService;

    public StatesResource(StatesService statesService, StatesRepository statesRepository, StatesQueryService statesQueryService) {
        this.statesService = statesService;
        this.statesRepository = statesRepository;
        this.statesQueryService = statesQueryService;
    }

    /**
     * {@code POST  /states} : Create a new states.
     *
     * @param states the states to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new states, or with status {@code 400 (Bad Request)} if the states has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/states")
    public ResponseEntity<States> createStates(@RequestBody States states) throws URISyntaxException {
        log.debug("REST request to save States : {}", states);
        if (states.getId() != null) {
            throw new BadRequestAlertException("A new states cannot already have an ID", ENTITY_NAME, "idexists");
        }
        States result = statesService.save(states);
        return ResponseEntity
            .created(new URI("/api/states/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /states/:id} : Updates an existing states.
     *
     * @param id the id of the states to save.
     * @param states the states to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated states,
     * or with status {@code 400 (Bad Request)} if the states is not valid,
     * or with status {@code 500 (Internal Server Error)} if the states couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/states/{id}")
    public ResponseEntity<States> updateStates(@PathVariable(value = "id", required = false) final Long id, @RequestBody States states)
        throws URISyntaxException {
        log.debug("REST request to update States : {}, {}", id, states);
        if (states.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, states.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!statesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        States result = statesService.save(states);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, states.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /states/:id} : Partial updates given fields of an existing states, field will ignore if it is null
     *
     * @param id the id of the states to save.
     * @param states the states to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated states,
     * or with status {@code 400 (Bad Request)} if the states is not valid,
     * or with status {@code 404 (Not Found)} if the states is not found,
     * or with status {@code 500 (Internal Server Error)} if the states couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/states/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<States> partialUpdateStates(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody States states
    ) throws URISyntaxException {
        log.debug("REST request to partial update States partially : {}, {}", id, states);
        if (states.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, states.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!statesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<States> result = statesService.partialUpdate(states);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, states.getId().toString())
        );
    }

    /**
     * {@code GET  /states} : get all the states.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of states in body.
     */
    @GetMapping("/states")
    public ResponseEntity<List<States>> getAllStates(StatesCriteria criteria, Pageable pageable) {
        log.debug("REST request to get States by criteria: {}", criteria);
        Page<States> page = statesQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /states/count} : count all the states.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/states/count")
    public ResponseEntity<Long> countStates(StatesCriteria criteria) {
        log.debug("REST request to count States by criteria: {}", criteria);
        return ResponseEntity.ok().body(statesQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /states/:id} : get the "id" states.
     *
     * @param id the id of the states to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the states, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/states/{id}")
    public ResponseEntity<States> getStates(@PathVariable Long id) {
        log.debug("REST request to get States : {}", id);
        Optional<States> states = statesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(states);
    }

    /**
     * {@code DELETE  /states/:id} : delete the "id" states.
     *
     * @param id the id of the states to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/states/{id}")
    public ResponseEntity<Void> deleteStates(@PathVariable Long id) {
        log.debug("REST request to delete States : {}", id);
        statesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
