/**
 * View Models used by Spring MVC REST controllers.
 */
package com.neu.test.web.rest.vm;
